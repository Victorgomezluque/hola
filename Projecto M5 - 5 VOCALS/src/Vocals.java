import java.util.Scanner;

public class Vocals {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fraseNormal = null;
		char vocal = 0;
		int contador = 1;

		FraseConVocales(fraseNormal, vocal, contador);
		/*editado con Gitlab , salu2*/
	}

	public static String frase(String fraseNormal) {

		fraseNormal = sc.nextLine();
		return fraseNormal;

	}

	public static char Vocal(char vocal, int contador) {

		switch (contador) {
		case 1:
			vocal = 'a';
			contador++;
			break;
		case 2:
			vocal = 'e';
			contador++;
			break;

		case 3:
			vocal = 'i';
			contador++;
			break;

		case 4:
			vocal = 'o';
			contador++;
			break;

		case 5:
			vocal = 'u';
			contador++;
			break;

		}
		return vocal;

	}

	public static void FraseConVocales(String fraseNormal, char vocal, int contador) {
		fraseNormal = frase(fraseNormal);

		char[] fraseSeparada = new char[fraseNormal.length()];
		int i;

		while (contador != 6) {
			vocal = Vocal(vocal, contador);
			for (i = 0; i != fraseNormal.length(); i++) {

				fraseSeparada[i] = fraseNormal.charAt(i);

				switch (fraseSeparada[i]) {
				case 'a':
					fraseSeparada[i] = vocal;

					break;
				case 'e':
					fraseSeparada[i] = vocal;
					break;

				case 'i':
					fraseSeparada[i] = vocal;

					break;

				case 'o':
					fraseSeparada[i] = vocal;

					break;

				case 'u':
					fraseSeparada[i] = vocal;

					break;

				}
			}
			contador++;
			System.out.println("Frase amb la Vocal " + vocal + ":  ");
			
			for (i = 0; i < fraseNormal.length(); i++) {

				System.out.print(fraseSeparada[i]);
			}
			System.out.println(" ");
			System.out.println(" ");
		}

	}

}
